# user_friendly_api_errors

Ultra-simple Ruby pattern that addresses the common need to translate technical errors to user-friendly ones when integrating with a third party API.

### Usage

Define a User Friendly Error class to handle any particular group of errors, likely for a resource or responsibility.

Pass a Hash to **error_translation** where the keys are Regex objects, and the values are I18n key names. 

```ruby
  class FooBarUserFriendlyError < UserFriendlyApiErrors::Base
    i18n_namespace :foo
    error_translation Regexp.new(/Unknown input value.*order/i) =>
                        :order_number_not_found,
                      Regexp.new(/Unknown input attribute.*location/i) =>
                        :address_not_found
  end
```

Pass a path to **i18n_namespace** to configure where each error translation lives. For this example, your I18n entries would look like:

```yaml
en:
  foo:
    order_number_not_found: 'The order you are referencing doesn''t seem to exist.'
    address_not_found: 'The address wasn''t found, try a different one?'
```

Call the class like this:

```ruby
user_facing_error = FooBarUserFriendlyError.new(ugly_error).make_friendly
```

That gives you an error suitable to show a user.
