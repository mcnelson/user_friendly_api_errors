class UserFriendlyApiErrors
  class Base
    attr_reader :message

    class << self
      attr_accessor :error_lookup, :i18n_path
    end

    # Define on subclasses as:
    # error_translation Regexp.new(/foo bar/i) => :i18n_key
    def self.error_translation(value)
      self.error_lookup = value
    end

    # Define i18n path that contains the keys you specify with error_translation.
    # Include a :default key.
    def self.i18n_namespace(value)
      self.i18n_path = value
    end

    def initialize(message)
      @message = message
      verify_correct_config
    end

    def make_friendly
      I18n.t(get_key, scope: i18n_path, raise: true, default: :default)
    end

    def get_key
      error_lookup.lazy.detect do |match, friendly|
        return friendly if match =~ message
      end
    end

    def error_lookup
      self.class.error_lookup
    end

    def i18n_path
      self.class.i18n_path
    end

    private

    def verify_correct_config
      if self.class == UserFriendlyApiErrors::Base
        raise 'not designed to be instantiated directly, create subclass'
      end

      if !error_lookup || error_lookup.empty?
        raise 'define at least one match => i18n key via `error_translation`'
      end

      if !i18n_path || i18n_path.empty?
        raise 'define `18n_namespace` that contains the keys you specify with `error_translation`'
      end
    end
  end
end
