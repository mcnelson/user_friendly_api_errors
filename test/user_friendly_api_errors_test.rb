require 'minitest/autorun'
require 'i18n'

require_relative '../lib/base.rb'

class UserFriendlyApiErrorsTest < Minitest::Test
  class ExampleUserFriendlyError < UserFriendlyApiErrors::Base
    i18n_namespace :foo
    error_translation Regexp.new(/Unknown input value.*order/i) =>
                        :order_number_not_found,
                      Regexp.new(/Unknown input attribute.*location/i) =>
                        :address_not_found
  end

  class OtherExampleUserFriendlyError < UserFriendlyApiErrors::Base
    i18n_namespace :bar
    error_translation Regexp.new(/Derp/i) => :herp
  end

  class NoConfigUserFriendlyError < UserFriendlyApiErrors::Base
    i18n_namespace :foo
  end

  def setup
    I18n.load_path << 'test/en.yml'
    I18n.enforce_available_locales = false
  end

  def test_happy_path
    ugly_error = "Unknown input value '123456789' for `order_id`. Please contact us at 123-456-7890 for order assistance."
    result = ExampleUserFriendlyError.new(ugly_error).make_friendly

    assert_equal "The order you are referencing doesn't seem to exist.", result
  end

  def test_class_instance_variables
    ugly_error = "Derp"
    result = OtherExampleUserFriendlyError.new(ugly_error).make_friendly

    assert_equal "Derp", result
  end

  def test_no_configuration
    assert_raises(RuntimeError, /define at least one match/) do
      NoConfigUserFriendlyError.new("bla")
    end
  end
end
