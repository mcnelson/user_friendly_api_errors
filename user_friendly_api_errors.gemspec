Gem::Specification.new do |s|
  s.name        = 'user_friendly_api_errors'
  s.version     = '0.0.1'
  s.date        = '2018-09-23'
  s.summary     = "Pattern to translate technical errors to user-friendly ones."
  s.description =
    "Ruby pattern that addresses the common need to translate technical errors to \
     user-friendly ones when integrating with a third party API."
  s.authors     = ["Michael Nelson"]
  s.email       = 'michael@nelsonware.com'
  s.files       = `git ls-files`.split("\n")
  s.test_files  = `git ls-files -- test/*`.split("\n")
  s.homepage    = 'https://gitlab.com/mcnelson/user_friendly_api_errors'
  s.license     = 'MIT'

  s.require_paths = ["lib"]
  s.required_ruby_version = '>= 2.5.1'

  s.add_development_dependency "minitest", "~> 5.11.3"
  s.add_development_dependency "i18n"
end
